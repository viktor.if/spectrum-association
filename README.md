# Obscure Steamy

## Project description

Demonstration basic capabilities of **Ant Design** system

- Select
- Table
- Typography
- Modal
- Spin
- Date/Time picker
- Pagination
- Form

## Related resources

- [Antd docs](https://ant.design/components/overview/)

<!-- ## Runtime environment -->

<!-- ## Commands -->

<!-- ## Development environment -->

### Setup

`yarn install`

### Boot

`yarn run`

## Test environment

- Prettier `npx prettier --check "src/**/*.ts"`

<!-- ## Deployment instructions -->
