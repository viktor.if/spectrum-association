import { Divider, Tag, Typography } from 'antd';

import { useFetch } from 'hooks/useFetch';
import { useEffect } from 'react';
import { useState } from 'react';
import { IAlbum } from 'types';

const { Title, Text, Link, Paragraph } = Typography;

export const TypographyPage = () => {
  const [text, setText] = useState('');
  const { data } = useFetch<IAlbum>('albums');

  useEffect(() => {
    if (data.length) {
      setText(data[3].title);
    }
  }, [data.length]);

  if (!data.length) return null;

  return (
    <div className="layout_page">
      <Tag color="geekblue" className="align-left">
        Titles
      </Tag>
      <Title level={2}>{data[0].title}</Title>
      <Title level={4}>{data[0].title}</Title>

      <Divider />

      <Tag color="geekblue" className="align-left">
        Text
      </Tag>
      <Text>{data[1].title}</Text>
      <Text strong>{data[1].title}</Text>
      <Text underline>{data[1].title}</Text>
      <Text mark>{data[1].title}</Text>
      <Text disabled>{data[1].title}</Text>
      <Text type="success">{data[1].title}</Text>

      <Divider />

      <Tag color="geekblue" className="align-left">
        Links
      </Tag>
      <Link href="#">{data[2].title}</Link>
      <Link href="#" underline>
        {data[2].title}
      </Link>

      <Divider />

      <Tag color="geekblue" className="align-left">
        Paragraphs
      </Tag>
      <Paragraph>{data[3].title}</Paragraph>
      <Paragraph strong>{data[3].title}</Paragraph>
      <Paragraph
        editable={{
          onChange: (value) => setText(value),
          triggerType: ['icon', 'text'],
          tooltip: 'Click to edit',
        }}
      >
        {text}
      </Paragraph>
      <Paragraph copyable={{ tooltips: ['Click to copy', 'Done'] }}>
        {data[3].title}
      </Paragraph>
      <Paragraph
        ellipsis={{ rows: 2, expandable: true, symbol: 'show more' }}
        style={{ width: '300px' }}
      >
        {data[3].title} {data[3].title} {data[3].title}
      </Paragraph>
    </div>
  );
};
