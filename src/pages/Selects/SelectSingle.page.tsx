import { message, Select, Typography } from 'antd';
import { useState } from 'react';

import { superHeroList } from 'data';

export const SelectSinglePage = () => {
  const [heroSelected, setSelectedHero] = useState<string | undefined>();

  const onChange = (value: string) => {
    message.info(`You have selected ${value}`);
    setSelectedHero(value);
  };

  return (
    <div className="layout_page align-center">
      <Typography.Paragraph className="paragraph">
        Which is your favorite super hero?
      </Typography.Paragraph>

      <Select
        placeholder="Super hero"
        className="select"
        value={heroSelected}
        onChange={onChange}
      >
        {superHeroList.map((hero) => (
          <Select.Option key={hero}>{hero}</Select.Option>
        ))}
      </Select>
    </div>
  );
};
