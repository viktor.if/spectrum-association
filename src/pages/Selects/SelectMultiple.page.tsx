import { message, Select, Typography } from 'antd';
import { useState } from 'react';

import { superHeroList } from 'data';

export const SelectMultiplePage = () => {
  const [heroSelected, setSelectedHero] = useState<string[]>([]);

  const onChange = (value: string[]) => {
    setSelectedHero(value);
    if (value.length) {
      message.info(
        [
          'You have',
          value.length > heroSelected.length ? 'added' : 'removed',
          value[value.length - 1],
        ].join(' ')
      );
    }
  };

  return (
    <div className="layout_page align-center">
      <Typography.Paragraph className="paragraph">
        Which are your favorite super heroes?
      </Typography.Paragraph>

      <Select
        allowClear
        className="select"
        maxTagCount={5}
        mode="multiple"
        onChange={onChange}
        placeholder="Super heros"
        value={heroSelected}
      >
        {superHeroList.map((hero) => (
          <Select.Option key={hero}>{hero}</Select.Option>
        ))}
      </Select>
    </div>
  );
};
