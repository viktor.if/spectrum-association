import {
  Button,
  Form,
  Input,
  Select,
  DatePicker,
  Checkbox,
  message,
} from 'antd';

export const FormPage = () => {
  return (
    <div className="layout_page align-center">
      <Form
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 40 }}
        onFinish={(values) => {
          message.info('Registration succeeded!');
          console.log({ values });
        }}
      >
        <Form.Item
          name="fullName"
          label="Full name"
          rules={[
            { required: true, message: 'Required' },
            { whitespace: true },
            { min: 3, message: 'Must have at least 3 letters' },
          ]}
          hasFeedback
        >
          <Input placeholder="Full name" />
        </Form.Item>

        <Form.Item
          name="email"
          label="Email"
          rules={[
            { required: true, message: 'Required' },
            { type: 'email', message: 'Invalid' },
          ]}
          hasFeedback
        >
          <Input placeholder="Email" />
        </Form.Item>

        <Form.Item
          name="password"
          label="Password"
          rules={[
            { required: true, message: 'Required' },
            { min: 6, message: 'Must have at least 6 letters' },
            {
              validator: (_, value) =>
                value && value.includes('A')
                  ? Promise.resolve()
                  : Promise.reject('Mush include A letter'),
            },
          ]}
          hasFeedback
        >
          <Input.Password placeholder="Password" />
        </Form.Item>

        <Form.Item
          name="confirmPassword"
          label="Confirm password"
          dependencies={['password']}
          rules={[
            { required: true, message: 'Required' },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }

                return Promise.reject('Does not match password');
              },
            }),
          ]}
          hasFeedback
        >
          <Input.Password placeholder="Confirm password" />
        </Form.Item>

        <Form.Item
          name="gender"
          label="Gender"
          requiredMark="optional"
          hasFeedback
        >
          <Select placeholder="Gender">
            <Select.Option value="male">Male</Select.Option>
            <Select.Option value="female">Female</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item
          name="birthDate"
          label="Birth date"
          rules={[{ required: true, message: 'Required' }]}
          hasFeedback
        >
          <DatePicker
            picker="date"
            placeholder="Birth date"
            style={{ width: '100%' }}
          />
        </Form.Item>

        <Form.Item
          name="website"
          label="Website Url"
          rules={[
            { required: true, message: 'Required' },
            { type: 'url', message: 'Invalid' },
          ]}
          hasFeedback
        >
          <Input placeholder="Website Url" />
        </Form.Item>

        <Form.Item
          name="agreement"
          wrapperCol={{ span: 24 }}
          valuePropName="checked"
          rules={[
            {
              validator: (_, value) =>
                value ? Promise.resolve() : Promise.reject('Required'),
            },
          ]}
          hasFeedback
        >
          <Checkbox>
            Agree to our <a href="#">Terms and Conditions</a>
          </Checkbox>
        </Form.Item>

        <Form.Item wrapperCol={{ span: 24 }}>
          <Button type="primary" htmlType="submit" block>
            Register
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
