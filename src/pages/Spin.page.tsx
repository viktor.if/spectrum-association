import { Button, Space, Spin, Typography } from 'antd';

import { useFetch } from 'hooks/useFetch';
import { useState } from 'react';
import { IPost } from 'types';

const { Paragraph } = Typography;

export const SpinPage = () => {
  const [loading, setLoading] = useState(false);
  const { data } = useFetch<IPost>('posts');

  if (!data.length) return null;

  return (
    <div className="layout_page align-center">
      <Space direction="vertical" size={32} style={{ padding: '0 16px' }}>
        <Button
          type={loading ? 'default' : 'primary'}
          onClick={() => setLoading((prev) => !prev)}
        >
          {loading ? 'Stop' : 'Start'} Loading
        </Button>

        <Spin spinning={loading}>
          <Paragraph className="align-text-left">{data[0].body}</Paragraph>
          <Paragraph className="align-text-left">{data[1].body}</Paragraph>
          <Paragraph className="align-text-left">{data[2].body}</Paragraph>
          <Paragraph className="align-text-left">{data[3].body}</Paragraph>
          <Paragraph className="align-text-left">{data[4].body}</Paragraph>
          <Paragraph className="align-text-left">{data[5].body}</Paragraph>
          <Paragraph className="align-text-left">{data[6].body}</Paragraph>
          <Paragraph className="align-text-left">{data[7].body}</Paragraph>
          <Paragraph className="align-text-left">{data[8].body}</Paragraph>
          <Paragraph className="align-text-left">{data[9].body}</Paragraph>
        </Spin>
      </Space>
    </div>
  );
};
