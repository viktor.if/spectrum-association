import { Button, message, Table } from 'antd';
import { ColumnsType } from 'antd/lib/table';

import { useFetch } from 'hooks';
import { IAlbum } from 'types';

export const TablePage = () => {
  const { data = [] } = useFetch<IAlbum>('albums');

  const onButtonClick = (text: string) => {
    message.info(text);
  };

  const renderAlbum = (payload: string) => (
    <Button type="link" onClick={() => onButtonClick(payload)}>
      {payload}
    </Button>
  );

  const columns: ColumnsType<IAlbum> = [
    {
      title: 'Album',
      dataIndex: 'title',
      render: renderAlbum,
    },
    {
      title: 'User ID',
      dataIndex: 'userId',
    },
  ];

  return (
    <div className="layout_page">
      <Table
        className="table"
        columns={columns}
        dataSource={data}
        rowKey="id"
      />
    </div>
  );
};
