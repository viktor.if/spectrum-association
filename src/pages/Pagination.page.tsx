import { Typography, Table } from 'antd';
import { ColumnsType } from 'antd/lib/table';

import { useFetch } from 'hooks';
import { ITodo } from 'types';

const { Text } = Typography;

export const PaginationPage = () => {
  const { data = [], loading } = useFetch<ITodo>('todos');

  const columns: ColumnsType<ITodo> = [
    {
      title: 'Record ID',
      dataIndex: 'id',
      sorter: (recA, recB) => {
        if (recA.id < recB.id) return -1;
        if (recA.id > recB.id) return 1;
        return 0;
      },
    },
    {
      title: 'User ID',
      dataIndex: 'userId',
    },
    {
      title: 'Title',
      dataIndex: 'title',
      render: (title) => {
        return <Text>{title}</Text>;
      },
    },
    {
      title: 'Status',
      dataIndex: 'completed',
      render: (completed) => {
        return <Text>{completed ? 'Completed' : 'In progress'}</Text>;
      },
      filters: [
        {
          text: 'Completed',
          value: true,
        },
        {
          text: 'In progress',
          value: false,
        },
      ],
      onFilter: (value, record) => record.completed === value,
    },
  ];

  return (
    <div className="layout_page">
      <Table
        className="table"
        loading={loading}
        columns={columns}
        dataSource={data}
        rowKey="id"
      />
    </div>
  );
};
