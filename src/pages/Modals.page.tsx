import { Button, Typography, Modal, Space } from 'antd';

import { useFetch } from 'hooks/useFetch';
import { useState } from 'react';
import { IPost } from 'types';

const { Paragraph } = Typography;

export const ModalsPage = () => {
  const { data = [] } = useFetch<IPost>('posts');

  const [isBasicModalOpened, setIsBasicModalOpened] = useState(false);
  const [isModalWFooterOpened, setIsModalWFooterOpened] = useState(false);

  if (!data.length) return null;

  const openModalDialog = () => {
    Modal.success({
      title: 'Success Modal Dialog',
      content: (
        <div>
          <Paragraph>{data[2].body}</Paragraph>
        </div>
      ),
    });
  };

  return (
    <div className="layout_page align-center">
      <Space direction="vertical" size={16}>
        {/** Buttons */}
        <Button
          type="primary"
          onClick={() => setIsBasicModalOpened(true)}
          style={{ minWidth: 300 }}
        >
          Open basic modal
        </Button>
        <Button
          type="primary"
          onClick={() => setIsModalWFooterOpened(true)}
          style={{ minWidth: 300 }}
        >
          Open modal w/ custom footer
        </Button>
        <Button
          type="primary"
          onClick={openModalDialog}
          style={{ minWidth: 300 }}
        >
          Open modal dialog
        </Button>
      </Space>

      {/** Modals */}
      <Modal
        title="Basic Modal"
        open={isBasicModalOpened}
        onCancel={() => setIsBasicModalOpened(false)}
        onOk={() => setIsBasicModalOpened(false)}
      >
        <Paragraph>{data[1].body}</Paragraph>
      </Modal>

      <Modal
        title="Modal w/ Custom Footer"
        open={isModalWFooterOpened}
        onCancel={() => setIsModalWFooterOpened(false)}
        footer={[
          <Button
            type="default"
            onClick={() => setIsModalWFooterOpened(false)}
            key="yes"
          >
            Yes
          </Button>,
          <Button
            type="default"
            onClick={() => setIsModalWFooterOpened(false)}
            key="no"
          >
            Nope
          </Button>,
          <Button
            type="default"
            onClick={() => setIsModalWFooterOpened(false)}
            key="maybe"
          >
            Maybe
          </Button>,
        ]}
      >
        <Paragraph>{data[1].body}</Paragraph>
      </Modal>
    </div>
  );
};
