import { DatePicker, message, Typography } from 'antd';

import { useState } from 'react';

export const DataPickerPage = () => {
  const [date, setDate] = useState<moment.Moment | null>(null);

  const onDateChange = (value: moment.Moment | null, dateString: string) => {
    setDate(value);
    message.info(dateString);
  };

  return (
    <div className="layout_page align-center">
      <Typography.Paragraph className="paragraph">
        Select a date, please
      </Typography.Paragraph>
      <DatePicker picker="date" onChange={onDateChange} value={date} />
    </div>
  );
};
