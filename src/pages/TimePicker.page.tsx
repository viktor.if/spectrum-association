import { message, TimePicker, Typography } from 'antd';

import { useState } from 'react';

export const TimePickerPage = () => {
  const [date, setDate] = useState<moment.Moment | null>(null);

  const onDateChange = (value: moment.Moment | null, dateString: string) => {
    setDate(value);
    message.info(dateString);
  };

  return (
    <div className="layout_page align-center">
      <Typography.Paragraph className="paragraph">
        Select time, please
      </Typography.Paragraph>
      <TimePicker onChange={onDateChange} value={date} />
    </div>
  );
};
