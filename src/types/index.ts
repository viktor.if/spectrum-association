export interface IAlbum {
  userId: number;
  id: number;
  title: string;
}

export interface IPost {
  userId: number;
  id: number;
  title: string;
  body: string;
}

export interface ITodo {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
}