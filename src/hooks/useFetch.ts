import { useEffect, useState } from "react";

export const useFetch = <T>(src: string) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<T[]>([])
  const [error, setError] = useState(null)

  useEffect(() => {
    setLoading(true);
    fetch(`https://jsonplaceholder.typicode.com/${src}`)
      .then((resp) => resp.json())
      .then((json) => setData(json))
      .catch((err) => setError(err))
      .finally(() => setLoading(false));

  }, []);

  return { data, error, loading }
}