import { Outlet } from 'react-router-dom';

import { NavLink } from './Link';

export const Layout = () => {
  return (
    <div className="layout">
      <div className="layout_nav-bar">
        <NavLink to="/selects/single">Select Single</NavLink>
        <NavLink to="/selects/multiple">Select Multiple</NavLink>
        <NavLink to="/table">Table</NavLink>
        <NavLink to="/typography">Typography</NavLink>
        <NavLink to="/modals">Modals</NavLink>
        <NavLink to="/spin">Spin</NavLink>
        <NavLink to="/datepicker">Date Picker</NavLink>
        <NavLink to="/timepicker">Time Picker</NavLink>
        <NavLink to="/pagination">Pagination</NavLink>
        <NavLink to="/form">Form</NavLink>
      </div>
      <Outlet />
    </div>
  );
};
