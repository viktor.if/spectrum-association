import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';

import { Layout } from './Layout';
import {
  SelectSinglePage,
  SelectMultiplePage,
  TablePage,
  TypographyPage,
  ModalsPage,
  SpinPage,
  DataPickerPage,
  TimePickerPage,
  PaginationPage,
  FormPage,
} from 'pages';

export const Router = () => (
  <BrowserRouter>
    <Routes>
      <Route element={<Layout />}>
        <Route path="selects/single" element={<SelectSinglePage />} />
        <Route path="selects/multiple" element={<SelectMultiplePage />} />
        <Route path="table" element={<TablePage />} />
        <Route path="typography" element={<TypographyPage />} />
        <Route path="modals" element={<ModalsPage />} />
        <Route path="spin" element={<SpinPage />} />
        <Route path="datepicker" element={<DataPickerPage />} />
        <Route path="timepicker" element={<TimePickerPage />} />
        <Route path="pagination" element={<PaginationPage />} />
        <Route path="form" element={<FormPage />} />
        <Route path="*" element={<Navigate to="selects/single" replace />} />
      </Route>
    </Routes>
  </BrowserRouter>
);
