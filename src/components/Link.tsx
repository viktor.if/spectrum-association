import { Button } from 'antd';
import { useNavigate, useLocation } from 'react-router-dom';

export const NavLink = ({ children, to }: { to: string; children: string }) => {
  const { pathname } = useLocation();
  const navigate = useNavigate();

  const isCurrentPath = pathname === to;

  return (
    <Button
      type={isCurrentPath ? 'primary' : 'link'}
      onClick={() => navigate(to)}
      style={{ textAlign: 'left' }}
    >
      {children}
    </Button>
  );
};
